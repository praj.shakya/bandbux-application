package com.prajwal.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BandBuxApplication {

	public static void main(String[] args) {
		SpringApplication.run(BandBuxApplication.class, args);
	}

}
