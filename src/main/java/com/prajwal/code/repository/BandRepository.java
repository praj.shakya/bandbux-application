package com.prajwal.code.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prajwal.code.entity.Band;

@Repository
public interface BandRepository extends JpaRepository<Band, Integer> {
	@Modifying
	@Query(value = "insert into tbl_band_genre(band_id, genre_id) values(?,?)", nativeQuery = true)
	public int addBandGenre(int bandId, int genreId);
}
