package com.prajwal.code.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prajwal.code.entity.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {

	@Query(value = "select * from tbl_genre where id not in(select genre_id from tbl_band_genre where band_id = ?)", nativeQuery = true)
	public List<Genre> getGenreNotInBand(int id);
	
	@Query(value = "select * from tbl_genre where id in(select genre_id from tbl_band_genre where band_id = ?)", nativeQuery = true)
	public List<Genre> getGenreInBand(int id);
}
