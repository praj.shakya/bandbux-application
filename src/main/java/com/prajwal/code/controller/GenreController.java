package com.prajwal.code.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prajwal.code.core.controller.CRUDController;
import com.prajwal.code.entity.Genre;
import com.prajwal.code.repository.GenreRepository;

@Controller
@RequestMapping(value = "/genre")
public class GenreController extends CRUDController<Genre, Integer>{
	@Autowired
	public GenreRepository getGenreRepository;
	public GenreController() {
		viewPath=pathUri="genre";
		pageTitle="Genre";
	}
	
	@GetMapping(value = "/genre-not-in-band/{id}")
	@ResponseBody
	public List<Genre> getGenre(@PathVariable("id")int id) {
		return getGenreRepository.getGenreNotInBand(id);
	}
	
	@GetMapping(value = "/genre-in-band/{id}")
	@ResponseBody
	public List<Genre> getGenreInBand(@PathVariable("id")int id){
		return getGenreRepository.getGenreInBand(id);
	}

}
