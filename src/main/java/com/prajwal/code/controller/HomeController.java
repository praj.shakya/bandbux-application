package com.prajwal.code.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.prajwal.code.core.controller.SiteController;

@Controller
@RequestMapping(value = "/")
public class HomeController extends SiteController {

	public HomeController() {
		viewPath = "home";
	}
	@GetMapping
	public String index() {
		return viewPath+"/index";
	}
}
