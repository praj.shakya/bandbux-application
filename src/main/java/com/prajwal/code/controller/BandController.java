package com.prajwal.code.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prajwal.code.core.controller.CRUDController;
import com.prajwal.code.entity.Band;
import com.prajwal.code.repository.BandRepository;

@Controller
@RequestMapping(value = "/bands")
public class BandController extends CRUDController<Band, Integer> {

	@Autowired
	public BandRepository getBandRepository;
	
	public BandController() {
		viewPath = pathUri="bands";
		pageTitle="Bands";
		
	}
	@GetMapping(value = "/profile/{id}")
	public String index(@PathVariable("id")int id, Model model) {
		model.addAttribute("record", getBandRepository.findById(id).get());
		return "bands/profile";
		
	}
	
	@PostMapping(value = "/add-band-genre")
	@Transactional
	@ResponseBody
	public String addbandgenre(@RequestParam("bandId")int bandId,@RequestParam("genreId")int genreId) {
		getBandRepository.addBandGenre(bandId, genreId);
		return "success";
	}
	
	
	
}
