package com.prajwal.code.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.prajwal.code.master.entity.MasterEntity;

@Entity
@Table(name = "tbl_genre")
public class Genre extends MasterEntity {
	@Column(name = "name")
	private String name;
	
	public Genre() {
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
