package com.prajwal.code.entity;



import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import com.prajwal.code.master.entity.MasterEntity;

@Entity
@Table(name = "tbl_band")
public class Band extends MasterEntity {
	@Column(name = "name")
	private String name;
	@Column(name = "location")
	private String location;
	@Column(name = "phone")
	private String phone;
	@Column(name = "email")
	private String email;
	@Column(name = "cost")
	private int cost;
	
	@Column(name="established_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date establisedDate;
	
	@Column(name = "description")
	@Type(type = "text")
	private String description;
	@Column(name = "entry_date", updatable = false, insertable = false)
	private Date entryDate;
	@Column(name = "status")
	private boolean status;
	@JoinTable(name="tbl_band_genre", joinColumns={@JoinColumn(name="band_id", referencedColumnName="id")},
			inverseJoinColumns={@JoinColumn(name="genre_id", referencedColumnName="id")})
	@ManyToMany
	private List<Genre> genre;
	
	public Band() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Date getEstablisedDate() {
		return establisedDate;
	}
	public void setEstablisedDate(Date establisedDate) {
		this.establisedDate = establisedDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public List<Genre> getGenre() {
		return genre;
	}
	public void setGenre(List<Genre> genre) {
		this.genre = genre;
	}
}
