package com.prajwal.code.core.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

public class SiteController {
	protected String viewPath;
	protected String pathUri;
	protected String pageTitle;
	
	@ModelAttribute(value = "viewPath")
	public String getViewPath() {
		return viewPath;
	}
	@ModelAttribute(value = "pathUri")
	public String getPathUri() {
		return pathUri;
	}
	@ModelAttribute(value = "pageTitle")
	public String getPageTitle() {
		return pageTitle;
	}
}
